package MyTests;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import Database.BinaryTree;
import Database.Game;
import Database.Trophy;
import Database.User;
import static org.junit.Assert.*;

public class BinaryTreeTest {
    
    BinaryTree bt;
    
    protected Calendar dob;
    protected Calendar c;
    
    protected User joithead;
    protected User pedant;
    protected User starveling;
    protected User churlish;
    protected User pribbling;
    protected User bugbear;
    protected User pignut;
    protected User harpy;
    protected User moldwarp;
    protected User cullion;
    protected User extra;
    
    protected Game witness;
    protected Game cod;
    
    protected Trophy witnessT1;
    protected Trophy codT1;
    
    @Before
    public void initialise() {
        bt = new BinaryTree();
        
        dob = new GregorianCalendar(1980, 4, 23); // Users can have the same dob - we don't care for these tests
        c = new GregorianCalendar(2016, 1, 28); // Trophies can have the same date - we don't care for these tests

        witness = new Game("The Witness", new GregorianCalendar(2016, 1, 26), 14);
        cod = new Game("Call of Duty: Black Ops III", new GregorianCalendar(2015, 11, 6), 48);

        witnessT1 = new Trophy("Shady Trees", Trophy.Rank.GOLD, Trophy.Rarity.ULTRA_RARE, c, witness);
        codT1 = new Trophy("Platinum", Trophy.Rank.PLATINUM, Trophy.Rarity.ULTRA_RARE, c, cod);
               
        
        joithead = new User("joithead", dob, 7);
        pedant = new User("pedant", dob, 4);
        cullion = new User("cullion", dob, 2);
        moldwarp = new User("moldwarp", dob, 1);
        harpy = new User("harpy", dob, 5);
        starveling = new User("starveling", dob, 10);
        pignut = new User("pignut", dob, 8);
        bugbear = new User("bugbear", dob, 9);
        pribbling = new User("pribbling", dob, 12);
        churlish = new User("churlish", dob, 14);
        extra = new User("extra", dob, 3);
        
    }
    
    // Don't set this up for all tests as it's not always required.
    protected void initialiseComplexTree() {

        joithead.setLeft(pedant);
        joithead.setRight(starveling);
        pedant.setParent(joithead);
        pedant.setLeft(cullion);
        pedant.setRight(harpy);
        cullion.setParent(pedant);
        cullion.setLeft(moldwarp);
        moldwarp.setParent(cullion);
        harpy.setParent(pedant);
        starveling.setParent(joithead);
        starveling.setLeft(pignut);
        starveling.setRight(pribbling);
        pignut.setParent(starveling);
        pignut.setRight(bugbear);
        bugbear.setParent(pignut);
        pribbling.setParent(starveling);
        pribbling.setRight(churlish);
        churlish.setParent(pribbling);
        bt.root = joithead;
    }

    @Test(expected=IllegalArgumentException.class)
    public void beFriendWithNullArg() {
        bt.beFriend(null);
    }
    
    @Test
    public void befriendOneUser() {
        boolean added = bt.beFriend(joithead);
        assertTrue(added);
    }
    
    @Test
    public void befriendMultipleUsers() {
        boolean added = bt.beFriend(joithead);
        assertTrue(added);
        added = bt.beFriend(pedant);
        assertTrue(added);
        added = bt.beFriend(starveling);
        assertTrue(added);
        
        assertEquals(joithead, pedant.getParent());
        assertEquals(pedant, joithead.getLeft());
        assertEquals(starveling, joithead.getRight());
        assertNull(joithead.getParent());
        assertNull(pedant.getLeft());
        assertNull(pedant.getRight());
    }
    
    @Test
    public void befriendAlreadyHasUser() {
        bt.beFriend(joithead);
        bt.beFriend(pedant);
        bt.beFriend(starveling);
        
        boolean added = bt.beFriend(pedant);
        assertFalse(added);
    }
    
    @Test
    public void toStringWithNoUsers() {
        String s = bt.toString();
        assertEquals("null", s);
    }
    
    @Test
    public void toStringWithMultipleUsers() {
        bt.beFriend(joithead);
        bt.beFriend(pedant);
        bt.beFriend(starveling);
        
        String s = bt.toString();
        String pedantString = "User: pedant\n\nTrophies: \n\nGames: \nEmpty game list\n\nBirth Date: May 23, 1980";
        String joitheadString = "User: joithead\n\nTrophies: \n\nGames: \nEmpty game list\n\nBirth Date: May 23, 1980";
        String starvelingString = "User: starveling\n\nTrophies: \n\nGames: \nEmpty game list\n\nBirth Date: May 23, 1980";
        assertEquals(pedantString + "\n" + joitheadString + "\n" + starvelingString, s);
    }
    

    @Test(expected=IllegalArgumentException.class)
    public void addGameWithNullUsernameArg() {
        bt.addGame(null, witness);
    }

    @Test(expected=IllegalArgumentException.class)
    public void addGameWithNullGameArg() {
        bt.addGame("joithead", null);
    }
    
    @Test
    public void addGameToUser() {
        bt.beFriend(joithead);
        bt.beFriend(pedant);
        assertNull(joithead.getGames());
        
        bt.addGame("joithead", witness);
        assertNotNull(joithead.getGames());
        assertNotNull(joithead.getGames().getGame("The Witness"));
    }
    
    @Test
    public void addGameToNonExistantUser() {
        bt.beFriend(joithead);
        bt.beFriend(pedant);
        
        bt.addGame("invalid", witness);
        assertNull(joithead.getGames());
        assertNull(pedant.getGames());
    }

    @Test(expected=IllegalArgumentException.class)
    public void addTrophyWithNullUsernameArg() {
        bt.addTrophy(null, witnessT1);
    }

    @Test(expected=IllegalArgumentException.class)
    public void addTrophyWithTropyNullArg() {
        bt.addTrophy("joithead", null);
    }
    
    @Test
    public void addTrophyToUser() {
        bt.beFriend(joithead);
        bt.beFriend(pedant);
        assertNull(joithead.getTrophies());
        
        bt.addTrophy("joithead", witnessT1);
        assertNotNull(joithead.getTrophies());
        assertEquals(1, joithead.getTrophies().size());
    }
    
    @Test
    public void addTrophyToNonExistantUser() {
        bt.beFriend(joithead);
        bt.beFriend(pedant);
        
        bt.addTrophy("invalid", witnessT1);
        assertNull(joithead.getTrophies());
        assertNull(pedant.getTrophies());
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void countBetterPlayersWithNullParam() {
        bt.countBetterPlayers(null);
    }
    
    @Test
    public void countBetterPlayersToNonExistantUser() {
        bt.beFriend(pedant);
        
        // Note. joithead has not been added to tree, so it
        // won't be found resulting in a -1
        int count = bt.countBetterPlayers(joithead);
        assertEquals(-1, count);
    }
    
    @Test
    public void countBetterPlayersWithOneUser() {
        bt.beFriend(joithead);
        int count = bt.countBetterPlayers(joithead);
        assertEquals(0, count);
    }
    

    @Test
    public void countBetterPlayersThanTopUser() {
        bt.beFriend(joithead);
        bt.beFriend(churlish);          // level 14 (the highest of this group)
        bt.beFriend(starveling);
        bt.beFriend(pedant);
        int count = bt.countBetterPlayers(churlish);
        assertEquals(0, count);
    }
    
    @Test
    public void countBetterPlayersThanBottomUser() {
        bt.beFriend(joithead);
        bt.beFriend(churlish);          
        bt.beFriend(starveling);
        bt.beFriend(pedant);            // level 4 (lowest of this group)
        int count = bt.countBetterPlayers(pedant);
        assertEquals(3, count);
    }
    
    
    @Test(expected=IllegalArgumentException.class)
    public void countWorsePlayersWithNullParam() {
        bt.countWorsePlayers(null);
    }
    
    @Test
    public void countWorsePlayersToNonExistantUser() {
        bt.beFriend(pedant);
        
        // Note. joithead has not been added to tree, so it
        // won't be found resulting in a -1
        int count = bt.countWorsePlayers(joithead);
        assertEquals(-1, count);
    }
    
    @Test
    public void countWorsePlayersWithOneUser() {
        bt.beFriend(joithead);
        int count = bt.countWorsePlayers(joithead);
        assertEquals(0, count);
    }
    

    @Test
    public void countWorsePlayersThanTopUser() {
        bt.beFriend(joithead);
        bt.beFriend(churlish);          // level 14 (the highest of this group)
        bt.beFriend(starveling);
        bt.beFriend(pedant);
        int count = bt.countWorsePlayers(churlish);
        assertEquals(3, count);
    }
    
    @Test
    public void countWorsePlayersThanBottomUser() {
        bt.beFriend(joithead);
        bt.beFriend(churlish);          
        bt.beFriend(starveling);
        bt.beFriend(pedant);            // level 4 (lowest of this group)
        int count = bt.countWorsePlayers(pedant);
        assertEquals(0, count);
    }
    
    @Test
    public void mostPlatinumsWithNoUsers() {
        User best = bt.mostPlatinums();
        assertNull(best);
    }
    
    @Test
    public void mostPlatinumsWithNoPlatinumUsers() {
        bt.beFriend(joithead);
        bt.addTrophy("joithead", witnessT1);
        User best = bt.mostPlatinums();
        assertNull(best);
    }
    
    @Test
    public void mostPlatinumsWithOnlyOnePlatinumUser() {
        bt.beFriend(joithead);
        bt.addTrophy("joithead", codT1);
        User best = bt.mostPlatinums();
        assertNotNull(best);
        assertEquals("joithead", best.getUsername());
    }
    
    @Test
    public void mostPlatinumsWithOnePlatinumUserAmongstMany() {
        bt.beFriend(joithead);
        bt.addTrophy("joithead", codT1);    // Joithead has Platinum trophy
        bt.beFriend(churlish);              // Churlish has no trophies
        
        bt.beFriend(starveling);            
        bt.addTrophy("starveling", witnessT1);  // Starveling has Gold trophy
        User best = bt.mostPlatinums();
        assertEquals("joithead", best.getUsername());
    }
    
    @Test
    public void mostPlatinumsWithManyPlatinumUsersAmongstMany() {
        bt.beFriend(joithead);
        bt.addTrophy("joithead", codT1);    // Joithead has Platinum trophy
        bt.beFriend(churlish);              // Churlish has no trophies
        
        bt.beFriend(starveling);  
        bt.addTrophy("starveling", codT1);
        bt.addTrophy("starveling", witnessT1);  // Starveling has Platinum and Gold trophy
        User best = bt.mostPlatinums();
        assertEquals("starveling", best.getUsername());
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void levelUpWithNullArg() {
        bt.levelUp(null);
    }
    
    @Test
    public void levelUpUserNotFound() {
        bt.beFriend(joithead);
        bt.levelUp("INVALID");
        
        // Confirm joithead level hasn't been changed
        assertEquals(7, joithead.getLevel());
    }
    
    @Test
    public void levelUpOnlyOneUser() {
        bt.beFriend(joithead);
        bt.levelUp("joithead");
        
        // Confirm joithead level has changed by 1
        assertEquals(8, joithead.getLevel());
        

        bt.levelUp("joithead");
        assertEquals(9, joithead.getLevel());
        bt.levelUp("joithead");
        assertEquals(10, joithead.getLevel());
    }
    
    @Test
    public void levelUpComplexTreeOneUser() {
        initialiseComplexTree();        
        
        bt.levelUp("joithead");        
        // Confirm joithead level has changed by 1
        assertEquals(8, joithead.getLevel());
        // Confirm joithead still root
        assertEquals("joithead", bt.root.getUsername());
        assertEquals(new Double(8.1395617008).doubleValue(), bt.root.getKey(), 0.0001);

    }
    
    @Test
    public void levelUpComplexTreeOneUserTwice() {
        initialiseComplexTree();
        
        bt.levelUp("joithead");
        assertEquals(8, joithead.getLevel());
        
        bt.levelUp("joithead");
        assertEquals(9, joithead.getLevel());
        
        assertEquals("pignut", bt.root.getUsername());
        assertEquals("starveling", bt.root.getRight().getUsername());
        assertEquals("bugbear", bt.root.getRight().getLeft().getUsername());
        assertEquals("pribbling", bt.root.getRight().getRight().getUsername());
        assertEquals("starveling", bugbear.getParent().getUsername());
        assertEquals("bugbear", joithead.getParent().getUsername());
        assertEquals("bugbear", starveling.getLeft().getUsername());
        assertNull(joithead.getRight());
        assertNull(pignut.getParent());
        assertEquals("pedant", pignut.getLeft().getUsername());
        assertEquals("starveling", pignut.getRight().getUsername());
    }
    
    
    @Test
    public void levelUpComplexTreeCullion() {
        initialiseComplexTree();
        
        bt.levelUp("cullion");
        assertEquals(3, cullion.getLevel());
        
        bt.levelUp("cullion");
        assertEquals(4, cullion.getLevel());
        
        bt.levelUp("cullion");
        assertEquals(5, cullion.getLevel());
        
        assertEquals("pedant", bt.root.getLeft().getUsername());
        assertEquals("moldwarp", bt.root.getLeft().getLeft().getUsername());
        assertNull(bt.root.getLeft().getLeft().getRight());
        assertEquals("harpy", bt.root.getLeft().getRight().getUsername());
        assertEquals("cullion", bt.root.getLeft().getRight().getLeft().getUsername());
        
        assertEquals("harpy", cullion.getParent().getUsername());
    }
    
    @Test
    public void levelUpComplexTreeCullionAndExtra() {
        initialiseComplexTree();
        bt.beFriend(extra);        
        assertEquals("extra", bt.root.getLeft().getLeft().getRight().getUsername());
        
        bt.levelUp("cullion");
        assertEquals(3, cullion.getLevel());
        
        bt.levelUp("cullion");
        assertEquals(4, cullion.getLevel());
        
        bt.levelUp("cullion");
        assertEquals(5, cullion.getLevel());
        
        assertEquals("pedant", bt.root.getLeft().getUsername());
        assertEquals("extra", bt.root.getLeft().getLeft().getUsername());
        assertEquals("moldwarp", bt.root.getLeft().getLeft().getLeft().getUsername());
        assertEquals("cullion", bt.root.getLeft().getLeft().getRight().getUsername());
        assertEquals("harpy", bt.root.getLeft().getRight().getUsername());
        
        assertEquals("extra", cullion.getParent().getUsername());
           
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void deFriendWithNullArg() {
        bt.deFriend(null);
    }
    
    @Test
    public void deFriendWithUserNotInTree() {
        boolean removed = bt.deFriend(joithead);
        assertFalse(removed);
    }
    
    @Test
    public void deFriendOnlyUserInTree() {        
        bt.beFriend(joithead);
        assertEquals("joithead", bt.root.getUsername());
        boolean removed = bt.deFriend(joithead);
        assertTrue(removed);
        assertNull(bt.root);
    }
    
    @Test
    public void deFriendComplexTree() {
        initialiseComplexTree();

        assertTrue(bt.deFriend(harpy));
        assertNull(pedant.getRight());
        

        assertTrue(bt.deFriend(cullion));
        assertEquals(moldwarp, pedant.getLeft());
        assertEquals("moldwarp", bt.root.getLeft().getLeft().getUsername());
        
        assertTrue(bt.deFriend(starveling));
        assertEquals(pribbling.getKey(), joithead.getRight().getKey(), 0.01);
        assertEquals(pribbling.getUsername(), joithead.getRight().getUsername());
        assertEquals(pribbling.getTrophies(), joithead.getRight().getTrophies());
        
        assertTrue(bt.deFriend(joithead));
        assertEquals(pignut.getKey(), bt.root.getKey(), 0.01);
        assertEquals(pignut.getUsername(), bt.root.getUsername());
        assertEquals(pignut.getTrophies(), bt.root.getTrophies());
        assertEquals(pribbling.getUsername(), bt.root.getRight().getUsername());
        assertEquals(bugbear.getUsername(), bt.root.getRight().getLeft().getUsername());

    }
    
    @Test
    public void defriendUserWithNoRightLeftChild() {
       initialiseComplexTree();
        
        assertTrue(bt.deFriend(starveling));
        assertEquals(joithead.getRight().getUsername(), pribbling.getUsername());
        assertEquals(joithead.getRight(), pribbling);
        assertEquals(pribbling.getLeft().getUsername(), pignut.getUsername());
        assertEquals(pribbling.getLeft(), pignut);
        assertEquals(pribbling.getParent().getUsername(), joithead.getUsername());
        assertEquals(pribbling.getParent(), joithead);
        assertEquals(pignut.getParent().getUsername(), pribbling.getUsername());
        assertEquals(pignut.getParent(), pribbling);
    }
    
    @Test
    public void defriendUserWithRightLeftChild() {
        initialiseComplexTree();
        
        assertTrue(bt.deFriend(joithead));
        assertEquals(bt.root.getUsername(), pignut.getUsername());
        assertEquals(bt.root, pignut);
        assertEquals(pignut.getLeft().getUsername(), pedant.getUsername());
        assertEquals(pignut.getLeft(), pedant);
        assertEquals(pignut.getRight().getUsername(), starveling.getUsername());
        assertEquals(pignut.getRight(), starveling);
        assertEquals(starveling.getLeft().getUsername(), bugbear.getUsername());
        assertEquals(starveling.getLeft(), bugbear);
        assertEquals(pedant.getParent().getUsername(), pignut.getUsername());
        assertEquals(pedant.getParent(), pignut);
        
    }
    
    @Test
    public void defriendUserWithNoRightChild() {
        initialiseComplexTree();
        
        assertTrue(bt.deFriend(cullion));
        assertEquals(pedant.getLeft().getUsername(), moldwarp.getUsername());
        assertEquals(moldwarp.getParent().getUsername(), pedant.getUsername());
    }
    
    @Test
    public void defriendUserWithNoChildren() {
        initialiseComplexTree();
        
        assertEquals(cullion.getLeft().getUsername(), moldwarp.getUsername());
        assertTrue(bt.deFriend(moldwarp));
        assertNull(cullion.getLeft());
    }
    
    @Test
    public void defriendUserWithNoLeftChild() {
        initialiseComplexTree();
        
        assertTrue(bt.deFriend(pignut));
        assertEquals(starveling.getLeft().getUsername(), bugbear.getUsername());
        assertEquals(bugbear.getParent().getUsername(), starveling.getUsername());
    }
}
