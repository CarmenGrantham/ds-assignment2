package MyTests;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import Database.Game;

public class GameTest {

    private String name;
    private Calendar released;
    private int totalTrophies;

    @Before
    public void initialise() {
        name = "Assassin's Creed IV: Black Flag";
        released = new GregorianCalendar(2013, 10, 29);
        totalTrophies = 14;
    }
    
    @Test
    public void testConstructor() {        
        Game game = new Game(name, released, totalTrophies);
        assertNotNull(game);
    }
    
    @Test
    public void testToString() {
        Game game = new Game(name, released, totalTrophies);
        assertEquals("\"Assassin's Creed IV: Black Flag\", released on: Nov 29, 2013", game.toString());
    }
    
    @Test
    public void testGetters() {
        Game game = new Game(name, released, totalTrophies);
        assertEquals(name, game.getName());
        assertEquals(released, game.getReleased());
        assertEquals(totalTrophies, game.getTotalTrophies());
    }
}
