package MyTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
            BinaryTreeTest.class, 
            BTreeTest.class,
            GameListTest.class,
            GameTest.class,
            TrophyTest.class
        }
        )
public class TestSuite {

    
    
}
