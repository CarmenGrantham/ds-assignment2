package MyTests;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import Database.Game;
import Database.Trophy;

public class TrophyTest {

    private String name;
    private Trophy.Rank rank;
    private Trophy.Rarity rarity;
    private Calendar obtained;
    private Game game;

    @Before
    public void initialise() {
        name = "What Did You Call Me?";
        rank = Trophy.Rank.BRONZE;
        rarity = Trophy.Rarity.RARE;
        obtained = new GregorianCalendar(2014, 4, 4);
        game = new Game("inFamous: Second Son", new GregorianCalendar(2014, 3, 21), 48);
    }

    @Test
    public void testConstructor() {
        Trophy trophy = new Trophy(name, rank, rarity, obtained, game);
        assertNotNull(trophy);
    }
    
    @Test
    public void testGetters() {
        Trophy trophy = new Trophy(name, rank, rarity, obtained, game);
        assertEquals(name, trophy.getName());
        assertEquals(rank, trophy.getRank());
        assertEquals(rarity, trophy.getRarity());
        assertEquals(obtained, trophy.getObtained());
        assertEquals(game, trophy.getGame());
    }
    
    @Test
    public void testToString() {
        Trophy trophy = new Trophy(name, rank, rarity, obtained, game);
        assertEquals("\"What Did You Call Me?\", rank: BRONZE, rarity: RARE, obtained on: May 04, 2014", trophy.toString());
    }
}
