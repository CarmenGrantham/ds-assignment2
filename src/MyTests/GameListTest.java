package MyTests;

import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import Database.Game;
import Database.GameList;

public class GameListTest {

    private Game g1;
    private Game g2;
    private Game g3;
    private GameList gl;

    @Before
    public void initialise() {
        g1 = new Game("Assassin's Creed IV: Black Flag", new GregorianCalendar(2013, 10, 29), 10);
        g2 = new Game("Child of Light", new GregorianCalendar(2014, 4, 1), 24);
        g3 = new Game("Dragon Age: Inquisition", new GregorianCalendar(2014, 11, 18), 53);
        gl = new GameList(null);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testNullAddGame() {
        gl.addGame(null);
    }
    
    @Test
    public void testAddOneGame() {
        gl.addGame(g1);
        assertEquals("\"Assassin's Creed IV: Black Flag\", released on: Nov 29, 2013", gl.toString());
    }

    @Test
    public void testAddTwoGames() {
        gl.addGame(g1);
        gl.addGame(g2);
        assertEquals("\"Assassin's Creed IV: Black Flag\", released on: Nov 29, 2013\n"
                + "\"Child of Light\", released on: May 01, 2014", gl.toString());
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testNullGetGame() {
        gl.getGame(null);
    }
    
    @Test
    public void testGetGameValid() {
        gl.addGame(g1);
        gl.addGame(g2);
        Game game = gl.getGame("Child of Light");
        assertNotNull(game);
        assertEquals("Child of Light", game.getName());
    }
    
    @Test
    public void testGetGameNotFound() {
        gl.addGame(g1);
        gl.addGame(g2);
        Game game = gl.getGame("Not here");
        assertNull(game);
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testNullRemoveGameString() {
        gl.removeGame((String)null);
    }
    
    @Test
    public void testRemoveGameStringValid() {
        gl.addGame(g1);
        gl.addGame(g2);
        assertNotNull(gl.getGame("Child of Light"));
        
        gl.removeGame("Child of Light");
        assertNull(gl.getGame("Child of Light"));
    }
    
    @Test
    public void testRemoveGameStringNotFound() {
        gl.addGame(g1);
        gl.addGame(g2);
        assertNotNull(gl.getGame("Child of Light"));
        
        gl.removeGame("Not valid");
        // Is there another test???
        assertNotNull(gl.getGame("Child of Light"));
    }
    

    @Test(expected=IllegalArgumentException.class)
    public void testNullRemoveGameObject() {
        gl.removeGame((Game)null);
    }
    
    @Test
    public void testRemoveGameObjectValid() {
        gl.addGame(g1);
        gl.addGame(g2);
        assertNotNull(gl.getGame("Child of Light"));
        
        gl.removeGame(g2);
        assertNull(gl.getGame("Child of Light"));
        assertNotNull(gl.getGame("Assassin's Creed IV: Black Flag"));
    }

}
