package MyTests;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import Database.BTree;

public class BTreeTest {
    BinaryTreeTest btt;

    @Before
    public void initialise() {
        // Need all the objects made in BinaryTreeTest
        btt = new BinaryTreeTest();
        btt.initialise();
    }
    
    @Test
    public void convertToBTreeOneUser() {
        btt.bt.beFriend(btt.moldwarp);
        BTree bt = btt.bt.convertToBTree(btt.bt);
        assertEquals(btt.moldwarp.getUsername(), bt.root.slots[0].getUsername());
    }
    
    @Test
    public void convertToBTreeThreeUsers() {
        btt.bt.beFriend(btt.moldwarp);
        btt.bt.beFriend(btt.cullion);
        btt.bt.beFriend(btt.pedant);
        BTree bt = btt.bt.convertToBTree(btt.bt);
        
        assertEquals(btt.moldwarp.getUsername(), bt.root.slots[0].getUsername());
        assertEquals(btt.cullion.getUsername(), bt.root.slots[1].getUsername());
        assertEquals(btt.pedant.getUsername(), bt.root.slots[2].getUsername());
    }
    
    
    @Test
    public void convertToBTreeFourUsers() {
        btt.bt.beFriend(btt.moldwarp);
        btt.bt.beFriend(btt.cullion);
        btt.bt.beFriend(btt.pedant);
        btt.bt.beFriend(btt.harpy);
        BTree bt = btt.bt.convertToBTree(btt.bt);
        
        
        // assert bt.root.slots = { cullion, null, null }        
        assertEquals(btt.cullion, bt.root.slots[0]);
        assertNull(bt.root.slots[1]);
        assertNull(bt.root.slots[2]);
        

        // assert bt.root.children[0].slots {moldwarp, null, null}
        assertEquals(btt.moldwarp, bt.root.children[0].slots[0]);
        assertNull(bt.root.children[0].slots[1]);
        assertNull(bt.root.children[0].slots[2]);
        

        // assert bt.root.children[3].slots {pedant, harpy, null}
        assertEquals(btt.pedant, bt.root.children[1].slots[0]);     // <- Code from lecture tested [3], should have been [1]
        assertEquals(btt.harpy, bt.root.children[1].slots[1]);      // <- Code from lecture tested [3], should have been [1]
        assertNull(bt.root.children[1].slots[2]);                   // <- Code from lecture tested [3], should have been [1]
    }

    @Test
    public void convertToBTreeFiveUsers() {
        btt.bt.beFriend(btt.moldwarp);
        btt.bt.beFriend(btt.cullion);
        btt.bt.beFriend(btt.pedant);
        btt.bt.beFriend(btt.harpy);
        btt.bt.beFriend(btt.joithead);
        BTree bt = btt.bt.convertToBTree(btt.bt);
        
        // moldwarp, cullion, pedant, harpy, joithead
        assertEquals(btt.cullion, bt.root.slots[0]);
        assertNull(bt.root.slots[1]);
        assertNull(bt.root.slots[2]);
        assertEquals(btt.moldwarp, bt.root.children[0].slots[0]);
        assertNull(bt.root.children[0].slots[1]);
        assertNull(bt.root.children[0].slots[2]);
        assertEquals(btt.pedant, bt.root.children[1].slots[0]);
        assertEquals(btt.harpy, bt.root.children[1].slots[1]);
        assertEquals(btt.joithead, bt.root.children[1].slots[2]);
    }
    
    @Test
    public void convertToBTreeSixUsers() {
        btt.bt.beFriend(btt.moldwarp);
        btt.bt.beFriend(btt.cullion);
        btt.bt.beFriend(btt.pedant);
        btt.bt.beFriend(btt.harpy);
        btt.bt.beFriend(btt.joithead);
        btt.bt.beFriend(btt.pignut);
        BTree bt = btt.bt.convertToBTree(btt.bt);
        
        // moldwarp, cullion, pedant, harpy, joithead, pignut
        assertEquals(btt.cullion, bt.root.slots[0]);
        assertEquals(btt.harpy, bt.root.slots[1]);
        assertNull(bt.root.slots[2]);
        assertEquals(btt.moldwarp, bt.root.children[0].slots[0]);
        assertNull(bt.root.children[0].slots[1]);
        assertNull(bt.root.children[0].slots[2]);
        assertEquals(btt.pedant, bt.root.children[1].slots[0]);
        assertEquals(btt.joithead, bt.root.children[2].slots[0]);
        assertEquals(btt.pignut, bt.root.children[2].slots[1]);
        assertNull(bt.root.children[2].slots[2]);
    }
    
    @Test
    public void convertToBTreeEightUsers() {
        btt.bt.beFriend(btt.moldwarp);
        btt.bt.beFriend(btt.cullion);
        btt.bt.beFriend(btt.pedant);
        btt.bt.beFriend(btt.harpy);
        btt.bt.beFriend(btt.joithead);
        btt.bt.beFriend(btt.pignut);
        btt.bt.beFriend(btt.bugbear);
        btt.bt.beFriend(btt.starveling);
        
        BTree bt = btt.bt.convertToBTree(btt.bt);

        // moldwarp, cullion, pedant, harpy, joithead, pignut, bugbear, starveling
        assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
        assertEquals("Checking that root[1] set correctly", btt.harpy, bt.root.slots[1]);
        assertEquals("Checking that root[2] set correctly", btt.pignut, bt.root.slots[2]);
        assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
        assertEquals("Checking that root.middleLeft[0] set correctly", btt.pedant, bt.root.children[1].slots[0]);
        assertEquals("Checking that root.middleRight[0] set correctly", btt.joithead, bt.root.children[2].slots[0]);
        assertEquals("Checking that root.right[0] set correctly", btt.bugbear, bt.root.children[3].slots[0]);
        assertEquals("Checking that root.right[1] set correctly", btt.starveling, bt.root.children[3].slots[1]);

    }
    
    @Test
    public void convertToBTreeTenUsers() {
        btt.bt.beFriend(btt.moldwarp);
        btt.bt.beFriend(btt.cullion);
        btt.bt.beFriend(btt.pedant);
        btt.bt.beFriend(btt.harpy);
        btt.bt.beFriend(btt.joithead);
        btt.bt.beFriend(btt.pignut);
        btt.bt.beFriend(btt.bugbear);
        btt.bt.beFriend(btt.starveling);
        btt.bt.beFriend(btt.pribbling);
        btt.bt.beFriend(btt.churlish);
        
        BTree bt = btt.bt.convertToBTree(btt.bt);

        // moldwarp, cullion, pedant, harpy, joithead, pignut, bugbear, starveling, pribbling, churlish
        assertEquals("Checking that root[0] set correctly", btt.harpy, bt.root.slots[0]);
        assertEquals("Checking that root.left[0] set correctly", btt.cullion, bt.root.children[0].slots[0]);
        assertEquals("Checking that root.left.left[0] set correctly", btt.moldwarp, bt.root.children[0].children[0].slots[0]);
        assertEquals("Checking that root.left.right[0] set correctly", btt.pedant, bt.root.children[0].children[1].slots[0]);
        assertEquals("Checking that root.right[0] set correctly", btt.pignut, bt.root.children[1].slots[0]);
        assertEquals("Checking that root.right[1] set correctly", btt.starveling, bt.root.children[1].slots[1]);
        assertEquals("Checking that root.right.left[0] set correctly", btt.joithead, bt.root.children[1].children[0].slots[0]);
        assertEquals("Checking that root.right.middle[0] set correctly", btt.bugbear, bt.root.children[1].children[1].slots[0]);
        assertEquals("Checking that root.right.right[0] set correctly", btt.pribbling, bt.root.children[1].children[2].slots[0]);
        assertEquals("Checking that root.right.right[1] set correctly", btt.churlish, bt.root.children[1].children[2].slots[1]);
    }

//    @Test
//    public void convertToBTreeTest() {
//
//        btt.initialiseComplexTree();
//        
//        BTree bt = btt.bt.convertToBTree(btt.bt);
//        assertEquals(btt.moldwarp.getUsername(), bt.root.slots[0].getUsername());
//        assertEquals(btt.cullion.getUsername(), bt.root.slots[1].getUsername());
//        
//    }
}
