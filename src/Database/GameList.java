package Database;


/**
 * Class to represent a single linked-list of Database.Game objects.
 * Created for Data Structures, SP2 2016
 * @author James Baumeister
 * @author Carmen Grantham
 * @version 1.0
 */
public class GameList {
    
    public Game head;
	
    public GameList(Game game) {
        head = game;
    }
    
	/**
	 * Adds a new game to the Game linked list.
	 * @param game The game to be added
	 * @throws IllegalArgumentException if argument is null
	 */
	public void addGame(Game game) throws IllegalArgumentException {
		if (game == null) {
		    throw new IllegalArgumentException();
		}
		
		// All games must have unique names, or else they are not added
		Game otherGame = getGame(game.getName());
		if (otherGame != null) {
		    // Game name already exists, don't add it
		    return;
		}
		
		if (head == null) {
		    // Adding the first game, add it to head
		    head = game;
		} else {
		    // There are already games, add it to the end
	        Game lastGame = getLastGame();
	        lastGame.setNext(game);
		}
	}
	
	/**
	 * Gets the requested game from the list.
	 * @param name The name of the game to be found
	 * @return The matching game, or null if not found
	 * @throws IllegalArgumentException if argument is null
	 */
	public Game getGame(String name) throws IllegalArgumentException {
	    if (name == null) {
	        throw new IllegalArgumentException();
	    }
	    Game game = head;
	    while (game != null) {
	        if (game.getName().equals(name)) {
	            // Game with name has been found, return game
	            return game;
	        }
	        // Proceed to next game in the list.
	        game = game.getNext();
	    }
	    // Game not found, return null;
		return null;
	}
	
	/**
	 * Removes the matching game from the list.
	 * @param name The name of the game to be removed
	 * @throws IllegalArgumentException if argument is null
	 */
	public void removeGame(String name) throws IllegalArgumentException {
		if (name == null) {
		    throw new IllegalArgumentException();
		}
		Game game = head;
		Game previousGame = null;
		while (game != null) {
		    if (game.getName().equals(name)) {
		        // Found game to remove
		        
		        if (previousGame == null) {
		            // Only the head has a null previous game, so 
		            // replace head with the next game in list
		            head = game.getNext();
		        } else {
		            // Set previous game to the one after the game
		            // to be removed.
		            previousGame.setNext(game.getNext());
		        }
		        // Game has been deleted so exit method
		        return;
		    }
		    previousGame = game;
		    game = game.getNext();
		}
	}
	
	/**
	 * Removes the matching game from the list.
	 * @param name The game to be removed
	 * @throws IllegalArgumentException if argument is null
	 */
	public void removeGame(Game game) throws IllegalArgumentException {
		if (game == null) {
		    throw new IllegalArgumentException();
		}
		removeGame(game.getName());
	}
	
	/**
	 * Get the last game in the list.
	 * @return
	 */
	private Game getLastGame() {
	    Game game = head;
        while (game != null) {
            Game nextGame = game.getNext();
            if (nextGame == null) {
                // There is no next game, so we are at the end of the list.
                return game;
            }
            // Advance game to next game.
            game = nextGame;
        }
	    return game;
	}
	
	public String toString() {
	    StringBuffer buffer = new StringBuffer();
	    Game game = head;
	    if (game == null) {
	        // There are no games so return string that it's empty.
	        return "Empty game list";
	    }
	    while (game != null) {
	        // Add new line break after first item added
	        if (buffer.length() > 0) {
	            buffer.append("\n");
	        }
	        buffer.append(game.toString());
	        game = game.getNext();
	    }
		return buffer.toString();
	}
}
