package Database;

import java.util.ArrayList;
import java.util.List;

/**
 * Uses a binary search tree to store a database of
 * PlayStation users. Nodes are ordered by user unique key (see the
 * User class for more detail).
 * Created for Data Structures, SP2 2016
 * @author James Baumeister
 * @author Carmen Grantham
 * @version 1.0
 */
public class BinaryTree {
	
    public User root;
    
    private boolean addedFriend = false;
    
    private User deleteReturn;
    
	/**
	 * Making new friends is great. This method should add your new
	 * bestie to your database (tree). Remember that they should be
	 * added according to their key.
	 * @param friend The friend to be added
	 * @return true if  successfully added, false for all error cases
	 * @throws IllegalArgumentException if friend is null
	 */
	public boolean beFriend(User friend) throws IllegalArgumentException {
	    if (friend == null) {
	        throw new IllegalArgumentException();
	    }
	    root = beFriend(root, friend);
	    return addedFriend;
	}
	
	private User beFriend(User localRoot, User friend) {
        if (localRoot == null) {
            // No root, set it to new friend
            addedFriend = true;
            return friend;
        } else if (friend.getKey() == localRoot.getKey()) {
            // Key already exists, do not add
            addedFriend = false;
            return localRoot;
        } else if (friend.getKey() < localRoot.getKey()) {
            // Add user to left as friend key is < root key
            User leftUser = beFriend(localRoot.getLeft(), friend);
            localRoot.setLeft(leftUser);
            leftUser.setParent(localRoot);
            return localRoot;
        } else {
            // Add user to right as friend key is > root key
            User rightUser = beFriend(localRoot.getRight(), friend);
            localRoot.setRight(rightUser);
            rightUser.setParent(localRoot);
            return localRoot;
        }
	}
	
	/**
	 * Sometimes friendships don't work out. In those cases it's best
	 * to remove that "friend" altogether. This method should remove
	 * all trace of that "friend" in the database (tree).
	 * @param friend the "friend" to remove
	 * @return true if successfully removed, false for all error cases
	 * @throws IllegalArgumentException if "friend" is null
	 */
	public boolean deFriend(User friend) throws IllegalArgumentException {
	    if (friend == null) {
	        throw new IllegalArgumentException();
	    }
	    User deletedUser = delete(friend);
		return deletedUser != null;
	}
	
	private User delete(User target) {
	    root = delete(root, target);
	    return deleteReturn;
	}
	
	private User delete(User localRoot, User target) {
        if (localRoot == null) {
            // item is not in the tree
            deleteReturn = null;
            return localRoot;
        }
        
        if (target.getKey() < localRoot.getKey()) {
            // target.key is smaller than localRoot.key
            localRoot.setLeft(delete(localRoot.getLeft(), target));
            return localRoot;
        } else if (target.getKey() > localRoot.getKey()) {
            // target.key is greater than localRoot.key
            localRoot.setRight(delete(localRoot.getRight(), target));
            return localRoot;
        } else {
            // target is at localRoot
            deleteReturn = localRoot;
            if (localRoot.getLeft() == null) {
                // no left child so return right child
                if (localRoot.getRight() != null) {
                    localRoot.getRight().setParent(localRoot.getParent());
                }
                return localRoot.getRight();
            } else if (localRoot.getRight() == null) {
                // no right child so return left child
                if (localRoot.getLeft() != null) {
                    localRoot.getLeft().setParent(localRoot.getParent());
                }
                return localRoot.getLeft();
            } else {
                // User has 2 children, replace with in-order successor
                if (localRoot.getRight().getLeft() == null) {
                    // the right child has no left child so return right child
                    User parent = localRoot.getParent();
                    User rightChild = localRoot.getRight();
                    User leftChild = localRoot.getLeft();
                    
                    rightChild.setLeft(leftChild);
                    rightChild.setParent(parent);
                    leftChild.setParent(rightChild);
                    
                    // Return the replacement of deleted node - the right child
                    return rightChild;
                        
                } else {
                    // right child has a left child, use left most child as successor
                    User parent = localRoot.getParent();
                    User leftChild = localRoot.getLeft();
                    User successor = findSuccessor(localRoot.getRight());
                    User successorRightChild = successor.getRight();
                    User successorParent = successor.getParent();
                    
                    successor.setParent(parent);
                    successor.setLeft(leftChild);
                    successor.setRight(localRoot.getRight());
                    leftChild.setParent(successor);
                    
                    successorRightChild.setParent(successorParent);
                    successorParent.setLeft(successorRightChild);

                    // Return replacement of deleted node - the in-order successor
                    return successor;
                }
            } 
        }        
	}
	
	/**
	 * Get the left most user as the successor.
	 * @param parent The parent of the successor
	 * @return The left most user
	 */
	private User findSuccessor(User parent) {
        if (parent == null || parent.getLeft() == null) {
            // There is no left child so parent is the successor
            return parent;
        } else {
            return findSuccessor(parent.getLeft());
        }
	}
		
	/**
	 * In your quest to be the very best you need to know how many
	 * of your friends are ranked higher than you. This method should
	 * return the number of higher ranked users that the provided reference
	 * user, or zero if there are none (woot!).
	 * @param reference The starting point in the search
	 * @return Number of higher ranked users or -1 if user not found
	 * @throws IllegalArgumentException if reference is null
	 */
	public int countBetterPlayers(User reference) throws IllegalArgumentException {
	    if (reference == null) {
	        throw new IllegalArgumentException();
	    }
	    // If user not in tree return -1
	    if (!userInTree(reference)) {
	        return -1;
	    }
	    
	    return countBetterPlayers(root, reference.getLevel());
	}
	
	/**
	 * Get the number of players that are better than specified level
	 * @param localRoot The user to compare
	 * @param level The level that user must be greater than.
	 * @return Number of players better than specified level
	 */
	private int countBetterPlayers(User localRoot, int level) {
	    if (localRoot == null) {
	        return 0;
	    }
	    int count = 0;
	    // Increment count if localRoot is at a higher level
	    if (localRoot.getLevel() > level) {
	        count = 1;
	    }
	    
	    // Add count to total for users on the left and the right
	    return count + countBetterPlayers(localRoot.getLeft(), level) + countBetterPlayers(localRoot.getRight(), level);
	}
	
	/**
	 * Bragging rights are well earned, but it's good to be sure that you're actually
	 * better than those over whom you're lording your achievements. This method
	 * should find all those friends who have a lower level than you, or zero if
	 * there are none (you suck).
	 * @param reference The starting point in the search
	 * @return Number of lower ranked users
	 * @throws IllegalArgumentException if reference is null
	 */
	public int countWorsePlayers(User reference) throws IllegalArgumentException {
	    if (reference == null) {
            throw new IllegalArgumentException();
        }

        // If user not in tree return -1
        if (!userInTree(reference)) {
            return -1;
        }
        return countWorsePlayers(root, reference.getLevel());
	}
    
	/**
	 * Get the number of players that have a lower level than the one specified
	 * @param localRoot The user to compare
	 * @param level The level that the user must be less than
	 * @return Number of players with a lower level
	 */
    private int countWorsePlayers(User localRoot, int level) {
        if (localRoot == null) {
            return 0;
        }
        int count = 0;
        // Increment count if localRoot is at a lower level
        if (localRoot.getLevel() < level) {
            count = 1;
        }
        
        // Add count to total for users on the left and the right
        return count + countWorsePlayers(localRoot.getLeft(), level) + countWorsePlayers(localRoot.getRight(), level);
    }
	
	/**
	 * The best player may not necessarily be measured by who has the highest level.
	 * Platinum trophies are the holy grail, so it would be good to know who has the
	 * most. This method should return the user with the highest number of platinum trophies.
	 * @return the user with the most platinum trophies, or null if there are none
	 */
	public User mostPlatinums() {
	    
	    if (root != null) {
	        return mostPlatinums(root, null);
	        
	    }
		return null;
	}
	
	/**
	 * Find the user with the most Platinum trophies
	 * @param localRoot The user to compare
	 * @param highestPlatinumUser The current highest Platinum User
	 * @return User with the most Platinum trophies.
	 */
	private User mostPlatinums(User localRoot, User highestPlatinumUser) {
	    if (localRoot == null) {
	        return highestPlatinumUser;
	    }
	    
	    // Get the number of platinum trophies for user, it must be larger than
	    // 0 to be even considered.
	    int localRootTrophies = localRoot.getPlatinumTrophyCount();
	    if (localRootTrophies > 0) {
	        if (highestPlatinumUser == null) {
	            // There is currently no highest user, so set it to current user.
	            highestPlatinumUser = localRoot;
	        } else {
	            int highestTrophies = highestPlatinumUser.getPlatinumTrophyCount();
	            // Does this user have more Platinum trophies than the currently set 
	            // highest user
	            if (localRootTrophies > highestTrophies) {
	                highestPlatinumUser = localRoot;
	            } else if (localRootTrophies == highestTrophies) {
	                // Both users have same number of platinum trophies
	                // Determine who has more gold trophies
	                int localRootGoldTrophies = localRoot.getGoldTrophyCount();
	                int highestGoldTrophies = highestPlatinumUser.getGoldTrophyCount();
	                // If local user has more trophies they are the new champion
	                // Note. If they have the same number of platinum and gold, then
	                // person first assigned as highest user remains so.
	                if (localRootGoldTrophies > highestGoldTrophies) {
	                    highestPlatinumUser = localRoot;
	                }
	            }
	            
	        }
	    }
	    
	    // Review users on the left and right of current user to see if there are
	    // any with more platinum trophies
	    highestPlatinumUser = mostPlatinums(localRoot.getLeft(), highestPlatinumUser);
	    highestPlatinumUser = mostPlatinums(localRoot.getRight(), highestPlatinumUser);
	    
	    return highestPlatinumUser;
	}
	
	/**
	 * You or one of your friends bought a new game! This method should add it to their
	 * GameList.
	 * @param username The user who has bought the game
	 * @param game The game to be added
	 */
	public void addGame(String username, Game game) throws IllegalArgumentException {
	    if (username == null || game == null) {
	        throw new IllegalArgumentException();
	    }
		User user = findUser(username);
		if (user != null) {
		    if (user.getGames() == null) {
		        // User doesn't have any games yet, so create new GameList
		        // with current game
		        user.setGames(new GameList(game));
		    } else {
		        // User already has games, so add to their list.
		        user.getGames().addGame(game);
		    }
		}
	}
	
	/**
	 * You or one of your friends achieved a new trophy! This method should add it to
	 * their trophies.
	 */
	public void addTrophy(String username, Trophy trophy) throws IllegalArgumentException {
        if (username == null || trophy == null) {
            throw new IllegalArgumentException();
        }
	    User user = findUser(username);
        if (user != null) {
            if (user.getTrophies() == null) {
                // Create new array list for trophies
                user.setTrophies(new ArrayList<Trophy>());
            }
            // Add trophy to their list.
            user.getTrophies().add(trophy);
        }
	}
	
	/**
	 * You or one of your friends has gained one level! This is great news, except that
	 * it may have ruined your tree structure! A re-balance may be in order.
	 * @param username The user whose level has increased
	 */
	public void levelUp(String username) throws IllegalArgumentException {
		if (username == null) {
		    throw new IllegalArgumentException();
		}
		User user = findUser(username);
		if (user != null) {
		    // Increase the users level by 1
		    user.setLevel(user.getLevel() + 1);
		    
		    // If the binary tree has become unbalanced it must be rebalanced.
		    if (leftUnbalanced(user) || rightUnbalanced(user)) {
		        rebalance(user);
		    }
		}
	}
	
	/**
	 * Check if the user that is on the left has a larger value than it's
	 * parent. If it is then it is deemed unbalanced.
	 * @param user The user to check
	 * @return True if user is on the left of it's parent and it has a larger
	 *         key than it's parent
	 */
	private boolean leftUnbalanced(User user) {
	    User parent = user.getParent();
	    if (parent != null) {
	        // User is on left and has larger key than parent
	        if (parent.getLeft() == user && user.getKey() > parent.getKey()) {
	            return true;
	        }
	    }
	    return false;
	}
	
	/**
	 * Check if user now has a key that is higher than the smallest
	 * value on the right of the binary tree.
	 * @param user The user with new key value
	 * @return True if user has a key with a higher value than the
	 *         smallest value on the right.
	 */
	private boolean rightUnbalanced(User user) {	    
	    // There is a right child
	    if (user.getRight() != null) {
	        // There is a right child with a smaller value
	        User successor = findSuccessor(user.getRight());
	        if (successor.getKey() < user.getKey()) {
	            return true;
	        }
	    }
	    
	    return false;
	}
	
	/**
	 * Binary Tree needs to be rebalanced due to the key of the
	 * specified user being changed.  
	 * @param user The user with the new key value
	 */
	private void rebalance(User user) {
	    // Replace the user with the inorder successor
	    User successor = findSuccessor(user.getRight());
	    
	    if (successor != null) {
	        // There is a successor, so they will take the position of the 
	        // user in the binary tree.
    	    if (successor.getParent() != user) {
    	        successor.getParent().setLeft(successor.getRight());
    	    }
    	    
    	    successor.setParent(user.getParent());
            successor.setLeft(user.getLeft());
            if (user.getRight() != successor) {
                successor.setRight(user.getRight());
            }
            
            if (user.getLeft() != null) {
                user.getLeft().setParent(successor);
            }
            if (user.getRight() != null) {
                user.getRight().setParent(successor);
            }
            
            
            replaceChild(user, successor);
	    } else {
	        // There is no right child (or successor)       
	        if (user.getLeft() != null) {
	            // Push up left child a level
	            replaceChild(user, user.getLeft());            
	        }
	    }
        
	    // Reset root to successor
        if (user == root) {
            root = successor;
        }
        
        // Clear all references to parent, left and right so that the beFriend
        // method adds the user properly
        user.setParent(null);
        user.setLeft(null);
        user.setRight(null);
        beFriend(user);
	}
	
	/**
	 * Replace the specified user with the child user.
	 * @param user The user to be replaced.
	 * @param child The child to replace the user
	 */
	private void replaceChild(User user, User child) {
	    // Update parent of child
	    child.setParent(user.getParent());
        if (user.getParent() != null) {
            // The user has a parent so update the parent's reference (either
            // left or right child) to this new child.
            if (user.getParent().getLeft() == user) {
                user.getParent().setLeft(child);
            } else if (user.getParent().getRight() == user) {
                user.getParent().setRight(child);
            }
        }
	}
		
	/**
	 * As you may have noticed, a BST is not always the best way to
	 * store some information. Another option is a B Tree. This method should take
	 * a BST and correctly convert it to a B Tree.
	 * @param tree The source BST
	 * @return The resulting B Tree
	 */
	public BTree convertToBTree(BinaryTree tree) throws IllegalArgumentException {
	    
	    List<User> friends = new ArrayList<User>();
	    // do in order traversal of BinaryTree
	    tree.inOrderTraverse(tree.root, friends);
	    
	    // Add sorted friends to BTree
	    BTree bTree = new BTree();
	    for (User friend: friends) {
	        bTree.add(friend);
	    }
	    
		return bTree;
	}
	
	/**
	 * Sort tree using inorder traversal method.
	 * @param localRoot The user at the root.
	 * @param friends The list of users (or friends)
	 */
	private void inOrderTraverse(User localRoot, List<User> friends) {

    	if (localRoot != null) {
            inOrderTraverse(localRoot.getLeft(), friends);
            friends.add(localRoot);
            inOrderTraverse(localRoot.getRight(), friends);
        }
	}
	
	/**
	 * Find the user with the specified username. 
	 * @param username The name of the user to find
	 * @return User object of user with name, or null if not found.
	 */
	private User findUser(String username) {
	    return findUser(root, username);
	}
	
	/**
	 * Find the user with the specified name starting at localRoot.
	 * @param localRoot The User to search.
	 * @param username The name of the user to search
	 * @return User object of the user with the name, or null if not found.
	 */
	private User findUser(User localRoot, String username) {
	    if (localRoot == null) {
	        return null;
	    }
	    
	    if (localRoot.getUsername().equals(username)) {
	        // Username matches, return User.
	        return localRoot;
	    }
	    
	    // Search for user down left side of tree
	    User leftSearch = findUser(localRoot.getLeft(), username);
	    if (leftSearch != null) {
	        return leftSearch;
	    }
	    // assert user not found on left, so check down right side
	    return findUser(localRoot.getRight(), username);
	}
	
	/**
	 * Check if user is in tree. If not found anywhere return false.
	 * 
	 * @param user The user to find
	 * @return True if user found in tree
	 */
	private boolean userInTree(User user) {
	    User foundUser = findUser(root, user);
	    return foundUser != null;
	}
	
	/**
	 * Search for user specified by target, looking at localRoot and then
	 * traversing down the left or right child if not found.
	 * @param localRoot The user to compare to
	 * @param target The user to find
	 * @return Found user object, or null if not found.
	 */
	private User findUser(User localRoot, User target) {
	    if (localRoot == null) {
	        return null;
	    }
	    if (target.getKey() == localRoot.getKey()) {
	        // Found user with same key (which means they are the same), return user.
	        return target;
	    } else if (target.getKey() < localRoot.getKey()) {
	        // Search down the left side of the binary tree
	        return findUser(localRoot.getLeft(), target);
	    } else {
	        // Search down the right side of the binary tree
	        return findUser(localRoot.getRight(), target);
	    }	    
	}
	
	/**
	 * A nice, neat print-out of your friends would look great in the secret scrap-book
	 * that you keep hidden underneath your pillow. This method should print out the
	 * details of each user, traversing the tree in order.
	 * @return A string version of the tree
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if (root != null) {
		    inOrderTraverse(root, buffer);
		} else {
		    // Not specified, but if binary tree is empty return 'null'
		    buffer.append("null");
		}
		return buffer.toString();
	}
	
	/**
	 * Use an in order traversal to get string value of user's in binary tree.
	 * @param localRoot The user to process
	 * @param buffer The StringBuffer to add user string to.
	 */
	private void inOrderTraverse(User localRoot, StringBuffer buffer) {
	    if (localRoot != null) {
	        inOrderTraverse(localRoot.getLeft(), buffer);
            if (buffer.length() > 0) {
                // If users have already been added, add a newline character
                // before adding current user
                buffer.append("\n");
            }
            buffer.append(localRoot.toString());
	        inOrderTraverse(localRoot.getRight(), buffer);
	    }
	}
	
}
