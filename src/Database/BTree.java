package Database;

/**
 * This class represents a B Tree. It has been
 * intentionally left blank so you can implement it
 * with whichever instance variables, constructors, and
 * methods that you desire (in accordance with the spec
 * and marking program). Ignore this class if you do not
 * want to attempt the final assignment question.
 *
 * Created for Data Structures, SP2 2016
 * @author James Baumeister
 * @author Carmen Grantham
 * @version 1.0
 */
public class BTree {
	
	public BTreeNode root;
	
	// Using a fixed tree order of 4
	private static int order = 4;
	
	private User newParent;
	private BTreeNode newChild;
	
	public class BTreeNode {
		public User[] slots;
		public BTreeNode[] children;
		private BTreeNode parent;
		int size = 0;


		public BTreeNode() {
		    slots = new User[order - 1];
		    children = new BTreeNode[order];
		    size = 0;
		}
	}
	
	/**
	 * Construct a new BTree
	 */
	public BTree() {
	    root = null;
	}
	
	/**
	 * Add user to tree
	 * @param friend The user to add
	 * @return True if user added, false if not added
	 */
	public boolean add(User friend) {
	    if (root == null) {
	        // Adding first user to BTree, put it in the first slot
	        root = new BTreeNode();
	        root.slots[0] = friend;
	        root.size = 1;
	        return true;
	    }
	    
	    newChild = null;
	    boolean result = insert(root, friend);
	    if (newChild != null) {
	        BTreeNode newRoot = new BTreeNode();
	        newRoot.children[0] = root;
	        newRoot.children[1] = newChild;
	        newRoot.slots[0] = newParent;
	        newRoot.size = 1;
	        root = newRoot;
	    }
	    return result;
	}
	
	/**
	 * Add friend to to BTree using root as the starting spot to look for
	 * an available place.
	 * @param root The node to add to
	 * @param friend The user to insert
	 * @return True if friend added to BTree, otherwise return false.
	 */
	private boolean insert(BTreeNode root, User friend) {
	    int index = binarySearch(friend, root.slots, 0, root.size);
	    if (index != root.size && root.slots[index] == friend) {
	        // friend is already in the tree, so don't add
	        return false;
	    }
	    
	    if (root.children[index] == null) {
	        if (root.size < order - 1) {
	            // There is room to add friend to children array
	            insertIntoNode(root, index, friend, null);
	            newChild = null;
	        } else {
	            // No room to add friend so BTreeNode needs to be split
	            splitNode(root, index, friend, null);
	        }
	        return true;
	    } else {
	        boolean result = insert(root.children[index], friend);
	        if (newChild != null) {
	            if (root.size < order - 1) {
	                insertIntoNode(root, index, newParent, newChild);
	                newChild = null;
	            } else {
	                splitNode(root, index, newParent, newChild);
	            }
	        }
	        return result;
	    }
	}
	
	/**
	 * Search for user in a user array, using specified start and last index
	 * @param friend The user to find
	 * @param data The array of users to search through
	 * @param start The first index to look at in data array
	 * @param last The index in data array to look at must be less than this
	 * @return The index of the friend, if found, or an empty index to use
	 */
	private int binarySearch(User friend, User[] data, int start, int last) {
	    for (int i = start; i < last; i++){
	        if (data[i] == null) {
	            // If element is null this indicates that friend couldn't
	            // be found so use this position to insert into
	            return i;
	        } else if (data[i] == friend) {
	            // Found user, return it's index
	            return i;
	        }
	    }
	    
	    return last;
	}
	
	/**
	 * Insert user into BTreeNode node.
	 * @param node The node to add to
	 * @param index The position where new node will be placed
	 * @param user The user to insert
	 * @param child The right child of the user to be inserted
	 */
	private void insertIntoNode(BTreeNode node, int index, User user, BTreeNode child) {
	    for (int i = node.size; i > index; i--) {
	        node.slots[i] = node.slots[i - 1];
	        node.children[i + 1] = node.children[i];
	    }
	    
	    node.slots[index] = user;
	    node.children[index + 1] = child;
	    node.size++;
	}
	
	/**
	 * Split the node into two as it's not large enough to account for new user.
	 * @param node The node to split
	 * @param index The position of the insertion
	 * @param user The user to insert
	 * @param child The right hand child of the user to insert
	 */
	private void splitNode(BTreeNode node, int index, User user, BTreeNode child) {
	    // Create a new child
	    newChild = new BTreeNode();
	    
	    // Determine number of items to move
	    int numToMove = (order - 1) - ((order - 1) / 2);
	    
	    // If insertion point is in the right half, move one less item
	    if (index > (order - 1) / 2) {
	        numToMove--;
	    }
	    
	    // Move items and their children
	    System.arraycopy(node.slots, order - numToMove -1, newChild.slots, 0, numToMove);
	    System.arraycopy(node.children, order - numToMove, newChild.children, 1, numToMove);
	    
	    node.size = order - numToMove - 1;
	    newChild.size = numToMove;
	    
	    // Insert new item
	    if (index == ((order - 1) / 2)) {      // Insert as middle item
	        newParent = user;
	        newChild.children[0] = child;
	    } else {
	        if (index < ((order - 1) / 2)) {   // Insert into the left
	            insertIntoNode(node, index, user, child);
	        } else {
	            insertIntoNode(newChild, index - ((order - 1) / 2) - 1, user, child);
	        }
	        
	        // The rightmost item of the node is the new parent
	        newParent = node.slots[node.size - 1];
	        
	        // Its child is the left child of the split of node
	        newChild.children[0] = node.children[node.size];
	        node.size--;	        
	    }
	    
	    // Remove items and references to moved items
	    for (int i = node.size; i < node.slots.length; i++) {
	        node.slots[i] = null;
	        node.children[i + 1] = null;
	    }
	
	}
}
