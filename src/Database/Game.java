package Database;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Class to represent a PlayStation game.
 * Created for Data Structures, SP2 2016
 * @author James Baumeister
 * @author Carmen Grantham
 * @version 1.0
 */
public class Game {

    private String name;
    private Calendar released;
    private Game next;
    private int totalTrophies;
    
    public Game(String name, Calendar released, int trophies) {
        this.name = name;
        this.released = released;
        this.totalTrophies = trophies;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getReleased() {
        return released;
    }

    public void setReleased(Calendar released) {
        this.released = released;
    }

    public Game getNext() {
        return next;
    }

    public void setNext(Game next) {
        this.next = next;
    }

    public int getTotalTrophies() {
        return totalTrophies;
    }

    public void setTotalTrophies(int totalTrophies) {
        this.totalTrophies = totalTrophies;
    }

    @Override
    public String toString() {
        // Data format is May 01, 2014
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        
        return "\"" + name + "\", released on: " + formatter.format(released.getTime());
    }
}
