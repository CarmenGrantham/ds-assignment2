package Database;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Database.Trophy.Rank;

/**
 * Class to represent a PlayStation user.
 * Created for Data Structures, SP2 2016
 * @author James Baumeister
 * @author Carmen Grantham
 * @version 1.0
 */
public class User {
	
	private String username;
    private int level;
    private double key;
    private ArrayList<Trophy> trophies;
    private GameList games;
    private Calendar dob;
    private User parent;
    private User left;
    private User right;
	
    public User(String username, Calendar dob, int level) {
        this.username = username;
        this.dob = dob;
        this.level = level;
        this.key = calculateKey();
    }
    
	/**
     * DO NOT MODIFY THIS METHOD
     * This method uses the username and level to create a unique key.
     * As we don't want the username's hash to increase the level, it's first converted
     * to a floating point, then added to the level.
     * @return the unique key
     */
    public double calculateKey() {
        int hash = Math.abs(username.hashCode());
        // Calculate number of zeros we need
        int length = (int)(Math.log10(hash) + 1);
        // Make a divisor 10^length
        double divisor = Math.pow(10, length);
        // Return level.hash
        return level + hash / divisor;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        if (level != this.level) {
            this.level = level;
            // After changing level recalculate key as
            // level is used in the formula
            this.key = calculateKey();
        }
    }

    public double getKey() {
        return key;
    }

    public void setKey(double key) {
        this.key = key;
    }

    public ArrayList<Trophy> getTrophies() {
        return trophies;
    }
    
    /**
     * Get list of trophies User has with the specified rank
     * @param rank The rank to use
     * @return List of trophies with rank
     */
    public ArrayList<Trophy> getTrophies(Rank rank) {
        ArrayList<Trophy> trophiesByRank = new ArrayList<Trophy>();
        if (trophies != null) {
            for (Trophy trophy : trophies) {
                if (trophy.getRank() == rank) {
                    trophiesByRank.add(trophy);
                }
            }
        }
        return trophiesByRank;
    }
    
    /**
     * Get the number of trophies with the specified rank.
     * @param rank The rank to use
     * @return The number of trophies with the rank
     */
    public int getTrophyCount(Rank rank) {
        return getTrophies(rank).size();
    }
    
    /**
     * Get the number of platinum trophies.
     * @return The number of platinum trophies for user.
     */
    public int getPlatinumTrophyCount() {
        return getTrophyCount(Rank.PLATINUM);
    }
    
    /**
     * Get the number of Gold trophies.
     * @return The number of Gold trophies for user
     */
    public int getGoldTrophyCount() {
        return getTrophyCount(Rank.GOLD);
    }


    public void setTrophies(ArrayList<Trophy> trophies) {
        this.trophies = trophies;
    }

    public GameList getGames() {
        return games;
    }

    public void setGames(GameList games) {
        this.games = games;
    }

    public Calendar getDob() {
        return dob;
    }

    public void setDob(Calendar dob) {
        this.dob = dob;
    }

    public User getParent() {
        return parent;
    }

    public void setParent(User parent) {
        this.parent = parent;
    }

    public User getLeft() {
        return left;
    }

    public void setLeft(User left) {
        this.left = left;
    }

    public User getRight() {
        return right;
    }

    public void setRight(User right) {
        this.right = right;
    }
    
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("User: " + username + "\n\n");
        
        // Get list of trophies
        buffer.append("Trophies: \n");
        if (trophies != null) {
            for (Trophy trophy : trophies) {
                buffer.append(trophy.toString() + "\n");
            }
        }
        
        // Get list of games. If there are no games print "Empty game list"
        buffer.append("\nGames: \n");
        if (games != null) {
            buffer.append(games.toString());
        } else {
            buffer.append("Empty game list");
        }

        // Format of birth date is May 01, 2014
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
        buffer.append("\n\nBirth Date: " + formatter.format(dob.getTime()));
        
        return buffer.toString();
    }
}
