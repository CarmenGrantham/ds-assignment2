package Testing;

import Database.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BTreeTest extends DSUnitTesting {

	BinaryTreeTest btt;

	@Before
	public void initialise() {
		// Need all the objects made in BinaryTreeTest
		btt = new BinaryTreeTest();
		btt.initialise();
	}

	/**
	 * Incremental tests have been provided for you to test your logic as you build the tree. Just comment out / in
	 * the tests that you need. Remember that a B tree balances as it builds so tests that are correct in early
	 * stages will not be correct as more nodes are added. Good luck!
	 * http://www.cs.usfca.edu/~galles/visualization/BTree.html
	 */
	@Test
	public void convertToBTree() {
		AssignmentMarker.marks.put("BTree:convertToBTree", 16.0f);

		BTree bt = btt.bt.convertToBTree(btt.bt);

		// moldwarp, cullion, pedant
		//assertEquals("Checking that root[0] set correctly", btt.moldwarp, bt.root.slots[0]);
		//assertEquals("Checking that root[1] set correctly", btt.cullion, bt.root.slots[1]);
		//assertEquals("Checking that root[2] set correctly", btt.pedant, bt.root.slots[2]);

		// moldwarp, cullion, pedant, harpy
		//assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
		//assertNull("Checking that root[1] set correctly", bt.root.slots[1]);
		//assertNull("Checking that root[2] set correctly", bt.root.slots[2]);
		//assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
		//assertNull("Checking that root.left[1] set correctly", bt.root.children[0].slots[1]);
		//assertNull("Checking that root.left[2] set correctly", bt.root.children[0].slots[2]);
		//assertEquals("Checking that root.right[0] set correctly", btt.pedant, bt.root.children[3].slots[0]);
		//assertEquals("Checking that root.right[1] set correctly", btt.harpy, bt.root.children[3].slots[1]);
		//assertNull("Checking that root.right[2] set correctly", bt.root.children[3].slots[2]);

		// moldwarp, cullion, pedant, harpy, joithead
		//assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
		//assertNull("Checking that root[1] set correctly", bt.root.slots[1]);
		//assertNull("Checking that root[2] set correctly", bt.root.slots[2]);
		//assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
		//assertNull("Checking that root.left[1] set correctly", bt.root.children[0].slots[1]);
		//assertNull("Checking that root.left[2] set correctly", bt.root.children[0].slots[2]);
		//assertEquals("Checking that root.right[0] set correctly", btt.pedant, bt.root.children[1].slots[0]);
		//assertEquals("Checking that root.right[1] set correctly", btt.harpy, bt.root.children[1].slots[1]);
		//assertEquals("Checking that root.right[2] set correctly", btt.joithead, bt.root.children[1].slots[2]);

		// moldwarp, cullion, pedant, harpy, joithead, pignut
		//assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
		//assertEquals("Checking that root[1] set correctly", btt.harpy, bt.root.slots[1]);
		//assertNull("Checking that root[2] set correctly", bt.root.slots[2]);
		//assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
		//assertNull("Checking that root.left[1] set correctly", bt.root.children[0].slots[1]);
		//assertNull("Checking that root.left[2] set correctly", bt.root.children[0].slots[2]);
		//assertEquals("Checking that root.middle[0] set correctly", btt.pedant, bt.root.children[1].slots[0]);
		//assertEquals("Checking that root.right[0] set correctly", btt.joithead, bt.root.children[2].slots[0]);
		//assertEquals("Checking that root.right[1] set correctly", btt.pignut, bt.root.children[2].slots[1]);
		//assertNull("Checking that root.right[2] set correctly", bt.root.children[2].slots[2]);

		// moldwarp, cullion, pedant, harpy, joithead, pignut, bugbear
		//assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
		//assertEquals("Checking that root[1] set correctly", btt.harpy, bt.root.slots[1]);
		//assertNull("Checking that root[2] set correctly", bt.root.slots[2]);
		//assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
		//assertNull("Checking that root.left[1] set correctly", bt.root.children[0].slots[1]);
		//assertNull("Checking that root.left[2] set correctly", bt.root.children[0].slots[2]);
		//assertEquals("Checking that root.middle[0] set correctly", btt.pedant, bt.root.children[1].slots[0]);
		//assertEquals("Checking that root.right[0] set correctly", btt.joithead, bt.root.children[2].slots[0]);
		//assertEquals("Checking that root.right[1] set correctly", btt.pignut, bt.root.children[2].slots[1]);
		//assertEquals("Checking that root.right[2] set correctly", btt.bugbear, bt.root.children[2].slots[2]);

		// moldwarp, cullion, pedant, harpy, joithead, pignut, bugbear, starveling
		//assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
		//assertEquals("Checking that root[1] set correctly", btt.harpy, bt.root.slots[1]);
		//assertEquals("Checking that root[2] set correctly", btt.pignut, bt.root.slots[2]);
		//assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
		//assertEquals("Checking that root.middleLeft[0] set correctly", btt.pedant, bt.root.children[1].slots[0]);
		//assertEquals("Checking that root.middleRight[0] set correctly", btt.joithead, bt.root.children[2].slots[0]);
		//assertEquals("Checking that root.right[0] set correctly", btt.bugbear, bt.root.children[3].slots[0]);
		//assertEquals("Checking that root.right[1] set correctly", btt.starveling, bt.root.children[3].slots[1]);

		// moldwarp, cullion, pedant, harpy, joithead, pignut, bugbear, starveling, pribbling
		//assertEquals("Checking that root[0] set correctly", btt.cullion, bt.root.slots[0]);
		//assertEquals("Checking that root[1] set correctly", btt.harpy, bt.root.slots[1]);
		//assertEquals("Checking that root[2] set correctly", btt.pignut, bt.root.slots[2]);
		//assertEquals("Checking that root.left[0] set correctly", btt.moldwarp, bt.root.children[0].slots[0]);
		//assertEquals("Checking that root.middleLeft[0] set correctly", btt.pedant, bt.root.children[1].slots[0]);
		//assertEquals("Checking that root.middleRight[0] set correctly", btt.joithead, bt.root.children[2].slots[0]);
		//assertEquals("Checking that root.right[0] set correctly", btt.bugbear, bt.root.children[3].slots[0]);
		//assertEquals("Checking that root.right[1] set correctly", btt.starveling, bt.root.children[3].slots[1]);
		//assertEquals("Checking that root.right[2] set correctly", btt.pribbling, bt.root.children[3].slots[2]);

		// moldwarp, cullion, pedant, harpy, joithead, pignut, bugbear, starveling, pribbling, churlish
		assertEquals("Checking that root[0] set correctly", btt.harpy, bt.root.slots[0]);
		assertEquals("Checking that root.left[0] set correctly", btt.cullion, bt.root.children[0].slots[0]);
		assertEquals("Checking that root.left.left[0] set correctly", btt.moldwarp, bt.root.children[0].children[0].slots[0]);
		assertEquals("Checking that root.left.right[0] set correctly", btt.pedant, bt.root.children[0].children[1].slots[0]);
		assertEquals("Checking that root.right[0] set correctly", btt.pignut, bt.root.children[1].slots[0]);
		assertEquals("Checking that root.right[1] set correctly", btt.starveling, bt.root.children[1].slots[1]);
		assertEquals("Checking that root.right.left[0] set correctly", btt.joithead, bt.root.children[1].children[0].slots[0]);
		assertEquals("Checking that root.right.middle[0] set correctly", btt.bugbear, bt.root.children[1].children[1].slots[0]);
		assertEquals("Checking that root.right.right[0] set correctly", btt.pribbling, bt.root.children[1].children[2].slots[0]);
		assertEquals("Checking that root.right.right[1] set correctly", btt.churlish, bt.root.children[1].children[2].slots[1]);
	}
}
