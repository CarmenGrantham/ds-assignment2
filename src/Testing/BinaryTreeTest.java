package Testing;

import Database.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

public class BinaryTreeTest extends DSUnitTesting {
	BinaryTree bt;

	// Start testing setup (see assignment spec for tree diagrams)
	Game witness, fallout, cod, cod2, witcher, witcher2, witcher3, uncharted, uncharted2;
	GameList gl1, gl2, gl3, gl4;

	Calendar c;
	Trophy witnessT1, witnessT2, falloutT1, falloutT2, falloutT3, codT1, witcherT1, witcherT2, witcherT3, witcherT4, witcherT5,
			unchartedT1, unchartedT2;

	ArrayList<Trophy> tl1, tl2, tl3, tl4, tl5;

	Calendar dob;

	User joithead, pedant, cullion, moldwarp, harpy, starveling, pignut, bugbear, pribbling, churlish;

	/**
	 * There's a LOT of setting up to do here. Refer to the assignment spec for
	 * a diagram explaining how this tree is constructed.
	 */
	@Before
	public void initialise() {
		witness = new Game("The Witness", new GregorianCalendar(2016, 1, 26), 14);
		fallout = new Game("Fallout 4", new GregorianCalendar(2015, 11, 10), 51);
		cod = new Game("Call of Duty: Black Ops III", new GregorianCalendar(2015, 11, 6), 48);
		cod2 = new Game("Call of Duty: Black Ops III", new GregorianCalendar(2015, 11, 6), 48);
		witcher = new Game("The Witcher 3: Wild Hunt", new GregorianCalendar(2015, 5, 19), 66);
		witcher2 = new Game("The Witcher 3: Wild Hunt", new GregorianCalendar(2015, 5, 19), 66);
		witcher3 = new Game("The Witcher 3: Wild Hunt", new GregorianCalendar(2015, 5, 19), 66);
		uncharted = new Game("Uncharted: The Nathan Drake Collection", new GregorianCalendar(2015, 10, 1), 166);
		uncharted2 = new Game("Uncharted: The Nathan Drake Collection", new GregorianCalendar(2015, 10, 1), 166);

		gl1 = new GameList(cod); {
			gl1.addGame(witness);
			gl1.addGame(fallout);
		}
		gl2 = new GameList(witcher); {
			gl2.addGame(uncharted);
		}
		gl3 = new GameList(witcher2);
		gl4 = new GameList(witcher3); {
			gl4.addGame(uncharted2);
			gl4.addGame(cod2);
		}

		c = new GregorianCalendar(2016, 1, 28); // Trophies can have the same date - we don't care for these tests

		witnessT1 = new Trophy("Shady Trees", Trophy.Rank.GOLD, Trophy.Rarity.ULTRA_RARE, c, witness);
		witnessT2 = new Trophy("Keep", Trophy.Rank.SILVER, Trophy.Rarity.RARE, c, witness);
		falloutT1 = new Trophy("War Never Changes", Trophy.Rank.BRONZE, Trophy.Rarity.COMMON, c, fallout);
		falloutT2 = new Trophy("The Nuclear Option", Trophy.Rank.SILVER, Trophy.Rarity.UNCOMMON, c, fallout);
		falloutT3 = new Trophy("Prepared for the Future", Trophy.Rank.GOLD, Trophy.Rarity.UNCOMMON, c, fallout);
		codT1 = new Trophy("Platinum", Trophy.Rank.PLATINUM, Trophy.Rarity.ULTRA_RARE, c, cod);
		witcherT1 = new Trophy("Xenonaut", Trophy.Rank.BRONZE, Trophy.Rarity.RARE, c, witcher);
		witcherT2 = new Trophy("Walked the Path", Trophy.Rank.GOLD, Trophy.Rarity.VERY_RARE, c, witcher);
		witcherT3 = new Trophy("Humpty Dumpty", Trophy.Rank.BRONZE, Trophy.Rarity.VERY_RARE, c, witcher);
		witcherT4 = new Trophy("Can't Touch This!", Trophy.Rank.BRONZE, Trophy.Rarity.RARE, c, witcher);
		witcherT5 = new Trophy("Overkill", Trophy.Rank.SILVER, Trophy.Rarity.RARE, c, witcher);
		unchartedT1 = new Trophy("Expert Fortune Hunter", Trophy.Rank.BRONZE, Trophy.Rarity.UNCOMMON, c, uncharted);
		unchartedT2 = new Trophy("Platinum", Trophy.Rank.PLATINUM, Trophy.Rarity.ULTRA_RARE, c, uncharted);

		tl1 = new ArrayList<Trophy>(); {
			tl1.add(codT1);
			tl1.add(falloutT1);
			tl1.add(witnessT2);
		}
		tl2 = new ArrayList<Trophy>(); {
			tl2.add(falloutT1);
			tl2.add(falloutT2);
			tl2.add(falloutT3);
			tl2.add(witnessT2);
		}
		tl3 = new ArrayList<Trophy>(); {
			tl3.add(unchartedT1);
			tl3.add(unchartedT2);
			tl3.add(witcherT4);
			tl3.add(witcherT3);
			tl3.add(witcherT1);
		}
		tl4 = new ArrayList<Trophy>(); {
			tl4.add(witcherT1);
			tl4.add(witcherT2);
			tl4.add(witcherT5);
		}
		tl5 = new ArrayList<Trophy>(); {
			tl5.add(unchartedT2);
			tl5.add(codT1);
		}

		dob = new GregorianCalendar(1980, 4, 23); // Users can have the same dob - we don't care for these tests

		joithead = new User("joithead", dob, 7);
		pedant = new User("pedant", dob, 4);
		cullion = new User("cullion", dob, 2);
		moldwarp = new User("moldwarp", dob, 1);
		harpy = new User("harpy", dob, 5);
		starveling = new User("starveling", dob, 10);
		pignut = new User("pignut", dob, 8);
		bugbear = new User("bugbear", dob, 9);
		pribbling = new User("pribbling", dob, 12);
		churlish = new User("churlish", dob, 14);

		joithead.setGames(gl1);
		joithead.setTrophies(tl1);
		joithead.setLeft(pedant);
		joithead.setRight(starveling);


		pedant.setGames(gl2);
		pedant.setTrophies(tl3);
		pedant.setParent(joithead);
		pedant.setLeft(cullion);
		pedant.setRight(harpy);


		cullion.setGames(gl3);
		cullion.setTrophies(tl4);
		cullion.setParent(pedant);
		cullion.setLeft(moldwarp);


		moldwarp.setGames(gl3);
		moldwarp.setTrophies(tl4);
		moldwarp.setParent(cullion);


		harpy.setGames(gl2);
		harpy.setTrophies(tl3);
		harpy.setParent(pedant);


		starveling.setGames(gl1);
		starveling.setTrophies(tl2);
		starveling.setParent(joithead);
		starveling.setLeft(pignut);
		starveling.setRight(pribbling);


		pignut.setGames(gl4);
		pignut.setTrophies(tl5);
		pignut.setParent(starveling);
		pignut.setRight(bugbear);


		bugbear.setGames(gl2);
		bugbear.setTrophies(tl3);
		bugbear.setParent(pignut);


		pribbling.setGames(gl1);
		pribbling.setTrophies(tl1);
		pribbling.setParent(starveling);
		pribbling.setRight(churlish);


		churlish.setGames(gl2);
		churlish.setTrophies(tl4);
		churlish.setParent(pribbling);


		bt = new BinaryTree();
		bt.root = joithead;
	}

	@Test
	public void beFriendNullArg() {
		AssignmentMarker.marks.put("BinaryTree:beFriendNullArg", 1.0f);
		try {
			bt.beFriend(null);

			fail("Checking that beFriend throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void beFriendDuplicate() {
		AssignmentMarker.marks.put("BinaryTree:beFriendDuplicate", 1.0f);
		assertFalse("Checking that beFriend returns false when user already in tree (1)", bt.beFriend(harpy));
	}

	@Test
	public void beFriend() {
		AssignmentMarker.marks.put("BinaryTree:beFriend", 6.0f);
		BinaryTree emptyBT = new BinaryTree();
		User bob = new User("bob", dob, 6);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		assertTrue("Checking that beFriend returns true when adding to an empty tree", emptyBT.beFriend(bob));
		assertEquals("Checking that the root has been correctly set", bob, emptyBT.root);

		assertTrue("Checking that beFriend returns true when adding a new user (1)", bt.beFriend(bob));
		assertEquals("Checking that harpy's right variable correctly set", bob, harpy.getRight());
		assertEquals("Checking that bob's parent variable correctly set", harpy, bob.getParent());

		harpy.setRight(null);
		bob = new User("bob", dob, 11);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		assertTrue("Checking that beFriend returns true when adding a new user (2)", bt.beFriend(bob));
		assertEquals("Checking that pribbling's left variable correctly set", bob, pribbling.getLeft());
		assertEquals("Checking that bob's parent variable correctly set", pribbling, bob.getParent());
	}

	@Test
	public void deFriendNullArg() {
		AssignmentMarker.marks.put("BinaryTree:deFriendNullArg", 1.0f);
		try {
			bt.deFriend(null);

			fail("Checking that deFriend throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void deFriendNonExistent() {
		AssignmentMarker.marks.put("BinaryTree:deFriendNonExistent", 1.0f);
		User bob = new User("bob", dob, 6);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		assertFalse("Checking that deFriend returns false when user not in tree", bt.deFriend(bob));
	}

	@Test
	public void deFriend() {
		AssignmentMarker.marks.put("BinaryTree:deFriend", 7.0f);
		assertTrue("Checking that deFriend returns true when removing a user (1)", bt.deFriend(harpy));
		assertEquals("Checking that pedant's right variable correctly set", null, pedant.getRight());

		assertTrue("Checking that deFriend returns true when removing a user (2)", bt.deFriend(cullion));
		assertEquals("Checking that pedant's left variable correctly set", moldwarp, pedant.getLeft());

		assertTrue("Checking that deFriend returns true when removing a user (3)", bt.deFriend(starveling));
		assertEquals("Checking that joithead's right correctly set (1)", pribbling.getKey(), joithead.getRight().getKey(), 0.01);
		assertEquals("Checking that joithead's right correctly set (2)", pribbling.getUsername(), joithead.getRight().getUsername());
		assertEquals("Checking that joithead's right correctly set (3)", pribbling.getTrophies(), joithead.getRight().getTrophies());

		assertTrue("Checking that deFriend returns true when removing the root", bt.deFriend(joithead));
		assertEquals("Checking that pignut is the new root", pignut.getKey(), bt.root.getKey(), 0.01);
		assertEquals("Checking that pignut is the new root", pignut.getUsername(), bt.root.getUsername());
		assertEquals("Checking that pignut is the new root", pignut.getTrophies(), bt.root.getTrophies());
	}

	@Test
	public void countBetterPlayersNullArg() {
		AssignmentMarker.marks.put("BinaryTree:countBetterPlayersNullArg", 1.0f);
		try {
			bt.countBetterPlayers(null);

			fail("Checking that deFriend throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void countBetterPlayersNonExistent() {
		AssignmentMarker.marks.put("BinaryTree:countBetterPlayersNonExistent", 1.0f);
		User bob = new User("bob", dob, 6);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		assertEquals("Checking that countBetterPlayers returns -1 when user not in tree", -1, bt.countBetterPlayers(bob));
	}

	@Test
	public void countBetterPlayers() {
		AssignmentMarker.marks.put("BinaryTree:countBetterPlayers", 4.0f);
		assertEquals("Checking that countBetterPlayers returns the correct number (1)", 9, bt.countBetterPlayers(moldwarp));
		assertEquals("Checking that countBetterPlayers returns the correct number (2)", 0, bt.countBetterPlayers(churlish));
		assertEquals("Checking that countBetterPlayers returns the correct number (3)", 6, bt.countBetterPlayers(harpy));
		assertEquals("Checking that countBetterPlayers returns the correct number (4)", 2, bt.countBetterPlayers(starveling));

		User bob = new User("bob", dob, 5);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		harpy.setLeft(bob);
		bob.setParent(harpy);
		assertEquals("Checking that countBetterPlayers handles same-level users correctly", 6, bt.countBetterPlayers(bob));
	}

	@Test
	public void countWorsePlayersNullArg() {
		AssignmentMarker.marks.put("BinaryTree:countWorsePlayersNullArg", 1.0f);
		try {
			bt.countWorsePlayers(null);

			fail("Checking that countWorsePlayers throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void countWorsePlayersNonExistent() {
		AssignmentMarker.marks.put("BinaryTree:countWorsePlayersNonExistent", 1.0f);
		User bob = new User("bob", dob, 6);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		assertEquals("Checking that countWorsePlayers returns -1 when user not in tree", -1, bt.countWorsePlayers(bob));
	}

	@Test
	public void countWorsePlayers() {
		AssignmentMarker.marks.put("BinaryTree:countWorsePlayers", 4.0f);
		assertEquals("Checking that countWorsePlayers returns the correct number (1)", 0, bt.countWorsePlayers(moldwarp));
		assertEquals("Checking that countWorsePlayers returns the correct number (2)", 9, bt.countWorsePlayers(churlish));
		assertEquals("Checking that countWorsePlayers returns the correct number (3)", 3, bt.countWorsePlayers(harpy));
		assertEquals("Checking that countWorsePlayers returns the correct number (4)", 7, bt.countWorsePlayers(starveling));

		User bob = new User("haypy", dob, 5);
		bob.setGames(gl1);
		bob.setTrophies(tl1);
		harpy.setRight(bob);
		bob.setParent(harpy);
		assertEquals("Checking that countWorsePlayers handles same-level users correctly", 3, bt.countWorsePlayers(bob));
	}

	@Test
	public void mostPlatinums() {
		AssignmentMarker.marks.put("BinaryTree:mostPlatinums", 4.0f);
		assertEquals("Checking that mostPlatinums returns the correct user", pignut, bt.mostPlatinums());

		ArrayList<Trophy> tl6 = new ArrayList<Trophy>(); {
			tl6.add(unchartedT2);
			tl6.add(codT1);
			tl6.add(witcherT2);
		}
		harpy.setTrophies(tl6);

		assertEquals("Checking that mostPlatinums correctly resolves ties", harpy, bt.mostPlatinums());
	}

	@Test
	public void addGameNullArg() {
		AssignmentMarker.marks.put("BinaryTree:addGameNullArg", 1.0f);
		try {
			bt.addGame(null, null);

			fail("Checking that addGame throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
		try {
			bt.addGame("bob", null);

			fail("Checking that addGame throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
		try {
			bt.addGame(null, witcher);

			fail("Checking that addGame throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void addGame() {
		AssignmentMarker.marks.put("BinaryTree:addGame", 4.0f);
		Game cod3 = new Game("Call of Duty: Black Ops III", new GregorianCalendar(2015, 11, 6), 48);
		bt.addGame("harpy", cod3);

		assertEquals("Checking that addGame correctly adds a game", witcher, harpy.getGames().head);
		assertEquals("Checking that addGame correctly adds a game", uncharted, harpy.getGames().head.getNext());
		assertEquals("Checking that addGame correctly adds a game", cod3, harpy.getGames().head.getNext().getNext());
	}

	@Test
	public void addTrophyNullArg() {
		AssignmentMarker.marks.put("BinaryTree:addTrophyNullArg", 1.0f);
		try {
			bt.addTrophy(null, null);

			fail("Checking that addTrophy throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
		try {
			bt.addTrophy("bob", null);

			fail("Checking that addTrophy throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
		try {
			bt.addTrophy(null, witcherT1);

			fail("Checking that addTrophy throws an IllegalArgumentException when null arg supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void addTrophy() {
		AssignmentMarker.marks.put("BinaryTree:addTrophy", 4.0f);
		bt.addTrophy("bugbear", witcherT2);

		assertEquals("Checking that addTrophy correctly adds a trophy", unchartedT1, bugbear.getTrophies().get(0));
		assertEquals("Checking that addTrophy correctly adds a trophy", unchartedT2, bugbear.getTrophies().get(1));
		assertEquals("Checking that addTrophy correctly adds a trophy", witcherT4, bugbear.getTrophies().get(2));
		assertEquals("Checking that addTrophy correctly adds a trophy", witcherT3, bugbear.getTrophies().get(3));
		assertEquals("Checking that addTrophy correctly adds a trophy", witcherT1, bugbear.getTrophies().get(4));
		assertEquals("Checking that addTrophy correctly adds a trophy", witcherT2, bugbear.getTrophies().get(5));
	}

	@Test
	public void levelUpNullArgs() {
		AssignmentMarker.marks.put("BinaryTree:levelUpNullArgs", 1.0f);
		try {
			bt.levelUp(null);

			fail("Checking that levelUp throws IllegalArgumentException when null args are supplied");
		}
		catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void levelUp() {
		AssignmentMarker.marks.put("BinaryTree:levelUp", 7.0f);
		bt.levelUp("joithead");
		assertEquals("Checking that levelUp correctly modifies tree (1)", "joithead", bt.root.getUsername());

		bt.levelUp("joithead");
		assertEquals("Checking that levelUp correctly modifies tree (2)", "pignut", bt.root.getUsername());
		assertEquals("Checking that levelUp correctly modifies tree (3)", "joithead", bugbear.getLeft().getUsername());
	}

	@Test
	public void toStringTest() {
		AssignmentMarker.marks.put("BinaryTree:toString", 3.0f);
		assertEquals("Checking that toString returns the correct String", outputString, bt.toString());
	}

	// Ridiculously long return string
	private String outputString = "User: moldwarp\n" +
			"\n" +
			"Trophies: \n" +
			"\"Xenonaut\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\"Walked the Path\", rank: GOLD, rarity: VERY_RARE, obtained on: Feb 28, 2016\n" +
			"\"Overkill\", rank: SILVER, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: cullion\n" +
			"\n" +
			"Trophies: \n" +
			"\"Xenonaut\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\"Walked the Path\", rank: GOLD, rarity: VERY_RARE, obtained on: Feb 28, 2016\n" +
			"\"Overkill\", rank: SILVER, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: pedant\n" +
			"\n" +
			"Trophies: \n" +
			"\"Expert Fortune Hunter\", rank: BRONZE, rarity: UNCOMMON, obtained on: Feb 28, 2016\n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\"Can't Touch This!\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\"Humpty Dumpty\", rank: BRONZE, rarity: VERY_RARE, obtained on: Feb 28, 2016\n" +
			"\"Xenonaut\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\"Uncharted: The Nathan Drake Collection\", released on: Nov 01, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: harpy\n" +
			"\n" +
			"Trophies: \n" +
			"\"Expert Fortune Hunter\", rank: BRONZE, rarity: UNCOMMON, obtained on: Feb 28, 2016\n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\"Can't Touch This!\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\"Humpty Dumpty\", rank: BRONZE, rarity: VERY_RARE, obtained on: Feb 28, 2016\n" +
			"\"Xenonaut\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\"Uncharted: The Nathan Drake Collection\", released on: Nov 01, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: joithead\n" +
			"\n" +
			"Trophies: \n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\"War Never Changes\", rank: BRONZE, rarity: COMMON, obtained on: Feb 28, 2016\n" +
			"\"Keep\", rank: SILVER, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"Call of Duty: Black Ops III\", released on: Dec 06, 2015\n" +
			"\"The Witness\", released on: Feb 26, 2016\n" +
			"\"Fallout 4\", released on: Dec 10, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: pignut\n" +
			"\n" +
			"Trophies: \n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\"Uncharted: The Nathan Drake Collection\", released on: Nov 01, 2015\n" +
			"\"Call of Duty: Black Ops III\", released on: Dec 06, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: bugbear\n" +
			"\n" +
			"Trophies: \n" +
			"\"Expert Fortune Hunter\", rank: BRONZE, rarity: UNCOMMON, obtained on: Feb 28, 2016\n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\"Can't Touch This!\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\"Humpty Dumpty\", rank: BRONZE, rarity: VERY_RARE, obtained on: Feb 28, 2016\n" +
			"\"Xenonaut\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\"Uncharted: The Nathan Drake Collection\", released on: Nov 01, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: starveling\n" +
			"\n" +
			"Trophies: \n" +
			"\"War Never Changes\", rank: BRONZE, rarity: COMMON, obtained on: Feb 28, 2016\n" +
			"\"The Nuclear Option\", rank: SILVER, rarity: UNCOMMON, obtained on: Feb 28, 2016\n" +
			"\"Prepared for the Future\", rank: GOLD, rarity: UNCOMMON, obtained on: Feb 28, 2016\n" +
			"\"Keep\", rank: SILVER, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"Call of Duty: Black Ops III\", released on: Dec 06, 2015\n" +
			"\"The Witness\", released on: Feb 26, 2016\n" +
			"\"Fallout 4\", released on: Dec 10, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: pribbling\n" +
			"\n" +
			"Trophies: \n" +
			"\"Platinum\", rank: PLATINUM, rarity: ULTRA_RARE, obtained on: Feb 28, 2016\n" +
			"\"War Never Changes\", rank: BRONZE, rarity: COMMON, obtained on: Feb 28, 2016\n" +
			"\"Keep\", rank: SILVER, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"Call of Duty: Black Ops III\", released on: Dec 06, 2015\n" +
			"\"The Witness\", released on: Feb 26, 2016\n" +
			"\"Fallout 4\", released on: Dec 10, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980\n" +
			"User: churlish\n" +
			"\n" +
			"Trophies: \n" +
			"\"Xenonaut\", rank: BRONZE, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\"Walked the Path\", rank: GOLD, rarity: VERY_RARE, obtained on: Feb 28, 2016\n" +
			"\"Overkill\", rank: SILVER, rarity: RARE, obtained on: Feb 28, 2016\n" +
			"\n" +
			"Games: \n" +
			"\"The Witcher 3: Wild Hunt\", released on: Jun 19, 2015\n" +
			"\"Uncharted: The Nathan Drake Collection\", released on: Nov 01, 2015\n" +
			"\n" +
			"Birth Date: May 23, 1980";
}
